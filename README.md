# Displaying Visitors' Data On Your Webpage

The result is showing in this [link](http://visitor.epizy.com/?i=1). You will have to get your VIEW_ID from [ Google Developers](https://developers.google.com) and register an account on [Geoname](http://geonames.org/), put your **View ID** in **g_analytics** and **username** that you get from [Geoname](http://geonames.org/) in **data.php**

You will also need to create to create **service-account-credentials.json** from [Google Developers](https://developers.google.com)

Email me at lau55@purdue.edu if you are interested in this project
![Demo](demo.png)
