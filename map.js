

let today_visitors = document.getElementById('today_data');
today_visitors_from_php = `<h2 id="number_visitor">` + today_visitors_from_php + "</h2>"
today_visitors.insertAdjacentHTML('beforeend',today_visitors_from_php);

let mymap = L.map('mapid', {
  center:[0,0],
  zoom : 1.5,
});

if (json.lat == null){
  alert("GeoNames does not return any lat or long.");
}
let dark_mode = L.tileLayer( 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    source: ['a','b','c']
});

let light_mode = L.tileLayer( 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(mymap);

  let whitecircle = [];
  let redcircle = [];
  let whitegroup = [];
  let redgroup = []

for (let i = 0; i <json.location.length ; i++){

if(json.lat[i]!=null && json.long[i]!=null){
whitecircle[i] = L.circle([json.lat[i], json.long[i]],{
color: 'white',
fillColor: '#ffffff',
fillOpacity: 0.5,
radius: (5*(json.amount[i]*1000)^(1/2))
})
redcircle[i] = L.circle([json.lat[i], json.long[i]],{
color: 'red',
fillColor: '#ff0000',
fillOpacity: 0.5,
radius: (5*(json.amount[i]*1000)^(1/2))
})

whitegroup.push(whitecircle[i]);
redgroup.push(redcircle[i]);

whitecircle[i].bindPopup("<b>Amount of people: </b> " + json.amount[i] );
redcircle[i].bindPopup("<b>Amount of people: </b> " + json.amount[i] );
}


}

let white = L.featureGroup(whitegroup)
.on('mouseover', (e) =>{
  e.sourceTarget.openPopup();
})
.on('mouseout', (e) =>{
  e.sourceTarget.closePopup();
})

let red = L.featureGroup(redgroup)
.on('mouseover', (e) =>{
  e.sourceTarget.openPopup();
})
.on('mouseout', (e) =>{
  e.sourceTarget.closePopup();
}).addTo(mymap);





let lightmode = true;

let myBtn = document.getElementById('dark_light');

myBtn.addEventListener("click", function light_or_dark (){
  if(lightmode ){
    light_mode.remove(mymap);
    dark_mode.addTo(mymap);
    red.remove(mymap);
    white.addTo(mymap);
    lightmode = false;
  }
  else{
    dark_mode.remove(mymap);
    light_mode.addTo(mymap);
    white.remove(mymap);
    red.addTo(mymap);
    lightmode = true;
  }
});
