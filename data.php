<?php

include 'g_analytics.php';

$data = $output_data;
$today_data = $output_today_data;


$place_obj = new stdClass();
$ch = curl_init();
const api = "http://api.geonames.org/searchJSON?q=";
const max_rows = "&maxRows=1";
//insert your username
const username = "&username=";


foreach($data as $i => $item){
    $place_obj -> location[$i] =    $item->dimensions;
    $place_obj -> amount[$i] =    $item->metrics[0]->values[0];

    if($item->dimensions[0] == "(not set)" && $item->dimensions[1] == "(not set)"){
      $input = $item->dimensions[2];
    }
    elseif ($item->dimensions[0] == "(not set)"){
    $input = $item->dimensions[1]." ".$item->dimensions[2];
      }
  else{
    $input = $item->dimensions[0]." ".$item->dimensions[1]." ".$item->dimensions[2];
  }
    $input = urlencode($input);
    $url = api.$input.max_rows.username;

    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_HEADER,0);
    $output = curl_exec($ch);

    if($output == FALSE){
      echo "curl error" . curl_error($ch);
      }
    $obj = json_decode($output);


    $place_obj -> lat[$i] =    $obj->geonames[0]->lat;
    $place_obj -> long[$i] =   $obj->geonames[0]->lng;


  }

    curl_close($ch);
    $final_output_data = json_encode($place_obj);
    $final_today_data = $today_data;
 ?>
