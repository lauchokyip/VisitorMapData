<?php
include "data.php";
?>

<!DOCTYPE html>
<html>
<head>
<title> Web User </title>
<script type="text/javascript">let json = <?php echo $final_output_data ?>;</script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
crossorigin=""/>

<link rel="stylesheet" type="text/css" href="index.css"/>

<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
crossorigin=""></script>

<script type="text/javascript">let today_visitors_from_php = <?php echo $final_today_data ?>;</script>
<h1> People who have visited my website in 2019 </h1>
</br>
<h2 id="today_data"> Number of visitors today: </h2>
 <button id="dark_light" type="button">Dark or Light Mode </button>
</head>


 <body>

 <div id="mapid"></div>
 <style> #mapid { height: 80vh; } </style>
 <script src="map.js" type="text/javascript"></script>
</body>



</html>
